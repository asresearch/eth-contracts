const { MerkleTree } = require('merkletreejs');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
const { expect } = require('chai');
const { expectRevert } = require('openzeppelin-test-helpers');

const MerkleDropV2Factory = artifacts.require("MerkleDropV2Factory");
const MerkleDropV2 = artifacts.require('MerkleDropV2');
const TestBank = artifacts.require("TestBank");
const TestBankFactory = artifacts.require("TestBankFactory");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");


function keccak256_enpacked(addr, amount) {
    k = web3.utils.soliditySha3({t:'address', v:addr},{t:'uint256', v:amount});
    return toBuffer(k);
  }

contract("MerkelDrop", (accounts) =>{
    let md = {}

    let token = {}
    let bank = {}
    let elements = {}
    let merkleTree = {}
    let hole = "0x0000000000000000000000000000000000000000"
    it("init", async() =>{
      bank_factory = await TestBankFactory.deployed();
      tx = await bank_factory.newTestBank("test");
      bank = await TestBank.at(tx.logs[0].args.addr);

      tlist_factory = await TrustListFactory.deployed();

      tx = await tlist_factory.createTrustList(accounts.slice(0, 1))
      tlist = await TrustList.at(tx.logs[0].args.addr);

      token_factory = await ERC20TokenFactory.deployed()
      tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "RDS", 18, "RDS", true);
      token = await ERC20Token.at(tx.logs[0].args._cloneToken);
      await token.changeTrustList(tlist.address);
      await tlist.add_trusted(accounts[0])

      await token.generateTokens(bank.address, "9999999999999999999999999999999999999999")

      elements = [keccak256_enpacked(accounts[0], 100),
      keccak256_enpacked(accounts[1], 200),
      keccak256_enpacked(accounts[2], 300),
      keccak256_enpacked(accounts[3], 400)]

      merkleTree = new MerkleTree(elements, keccak256, { sortPairs: true });

      root = merkleTree.getHexRoot();

      md_factory = await MerkleDropV2Factory.deployed();
      tx = await md_factory.createMerkleDropV2("testMD", bank.address, token.address, root);
      md = await MerkleDropV2.at(tx.logs[0].args.addr);
      await md.enableUpdateWhenClaim(true);

      leaf = keccak256_enpacked(accounts[0], 100);

      proof = merkleTree.getHexProof(leaf);

      bool = await md.verify(accounts[0], 100, proof)
      expect(bool).to.equal(true);

      proof[0] = '0x1234'
      bool = await md.verify(accounts[0], 100, proof)
      expect(bool).to.equal(false);
    })

    it("test claim", async() =>{
      leaf = keccak256_enpacked(accounts[2], 300);
      proof = merkleTree.getHexProof(leaf);

      await expectRevert(md.claim(accounts[2], 300, proof), "invalid merkle proof")

      await md.claim(accounts[2], 300, proof, {from:accounts[2]});
      expect((await token.balanceOf(accounts[2])).toNumber()).to.equal(300);

      elements = [keccak256_enpacked(accounts[0], 100),
      keccak256_enpacked(accounts[1], 200),
      keccak256_enpacked(accounts[2], 0),
      keccak256_enpacked(accounts[3], 400)]

      merkleTree = new MerkleTree(elements, keccak256, { sortPairs: true });
      leaf = keccak256_enpacked(accounts[3], 400);
      proof = merkleTree.getHexProof(leaf);
      await md.claim(accounts[3], 400, proof, {from:accounts[3]});
      expect((await token.balanceOf(accounts[3])).toNumber()).to.equal(400);

      elements = [keccak256_enpacked(accounts[0], 100),
      keccak256_enpacked(accounts[1], 200),
      keccak256_enpacked(accounts[2], 0),
      keccak256_enpacked(accounts[3], 0)]

      merkleTree = new MerkleTree(elements, keccak256, { sortPairs: true });
      root = merkleTree.getHexRoot();
      expect(await md.merkle_root()).to.equal(root);
    })
    it("simple update", async() =>{
      leaf = keccak256_enpacked(accounts[0], 100);
      proof = merkleTree.getHexProof(leaf);
      await md.simpleUpdate(accounts[0], 100, accounts[0], 1000, proof);

      elements = [keccak256_enpacked(accounts[0], 1000),
      keccak256_enpacked(accounts[1], 200),
      keccak256_enpacked(accounts[2], 0),
      keccak256_enpacked(accounts[3], 0)]
      merkleTree = new MerkleTree(elements, keccak256, { sortPairs: true });
      root = merkleTree.getHexRoot();
      expect(await md.merkle_root()).to.equal(root);

      elements = [keccak256_enpacked(accounts[0], 1000),
      keccak256_enpacked(accounts[1], 200),
      keccak256_enpacked(accounts[2], 0),
      keccak256_enpacked(accounts[3], 0),
      keccak256_enpacked(accounts[4], 500),
      keccak256_enpacked(hole, 0),
      keccak256_enpacked(hole, 0),
      keccak256_enpacked(hole, 0)]
      merkleTree = new MerkleTree(elements, keccak256, { sortPairs: true });
      leaf = keccak256_enpacked(accounts[4], 500);
      proof = merkleTree.getHexProof(leaf);
      await expectRevert(md.simpleUpdate(accounts[4], 0, accounts[4], 500, proof), "invalid old info")
      await md.simpleUpdate(hole, 0, accounts[4], 500, proof);
      root = merkleTree.getHexRoot();
      expect(await md.merkle_root()).to.equal(root);

      leaf = keccak256_enpacked(accounts[0], 1000);
      proof = merkleTree.getHexProof(leaf);
      await md.claim(accounts[0], 1000, proof, {from:accounts[0]});
      await expectRevert(md.claim(accounts[0], 1000, proof, {from:accounts[0]}), "invalid merkle proof")

    })
})
