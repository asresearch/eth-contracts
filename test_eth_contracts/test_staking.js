const { expect } = require('chai');
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");

//const getBlockNumber = require('./blockNumber')(web3)
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const ERC20Staking = artifacts.require("ERC20Staking");
const ERC20StakingFactory = artifacts.require("ERC20StakingFactory");


contract("Test ", (accounts)=>{
  context("init", ()=>{
    let plt = {}
    let xplt = {}
    let pusd = {}
    let es = {}
    it("init", async function(){

      tlist_factory = await TrustListFactory.deployed();

      tx = await tlist_factory.createTrustList(accounts.slice(0, 1))
      tlist = await TrustList.at(tx.logs[0].args.addr);

      token_factory = await ERC20TokenFactory.deployed()
      tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "plt", 18, "PLT", true);
      plt = await ERC20Token.at(tx.logs[0].args._cloneToken);
      await plt.changeTrustList(tlist.address)

      tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "xplt", 18, "xPLT", true);
      xplt = await ERC20Token.at(tx.logs[0].args._cloneToken);
      await xplt.changeTrustList(tlist.address)

      await plt.generateTokens(accounts[0], '100000000000000000000000000')
      await plt.generateTokens(accounts[1], "100000000000000000000000000")
      await plt.generateTokens(accounts[2], "100000000000000000000000000")

      staking_factory = await ERC20StakingFactory.deployed();
      tx = await staking_factory.createERC20Staking(plt.address, xplt.address);

      es = await ERC20Staking.at(tx.logs[0].args.addr);
      await tlist.add_trusted(es.address);
    })


    it('staking', async function(){
      xpltaddr = await es.lp_token()
      xplt = await ERC20Token.at(xpltaddr)
      await plt.destroyTokens(accounts[0], await plt.balanceOf(accounts[0]))
      await plt.destroyTokens(accounts[1], await plt.balanceOf(accounts[1]))
      await plt.generateTokens(accounts[0], '100000000000000000000000000')
      await plt.generateTokens(accounts[1], "100000000000000000000000000")
      await plt.approve(es.address, '1000000000000000000000000', {from:accounts[1]})
      await plt.approve(es.address, '1000000000000000000000000', {from:accounts[0]})

      await es.stake('1000000000000000000000')
      l0 = (await xplt.balanceOf(accounts[0])).toString()
      expect(l0).to.equal('1000000000000000000000')

      await es.claim('1000000000000000000000')
      l0 = (await xplt.balanceOf(accounts[0])).toNumber()
      expect(l0).to.equal(0)

      await es.stake('1000000000000000000000')
      await plt.generateTokens(es.address, '1000000000000000000000')
      await es.claim('1000000000000000000000')
      b0 = (await plt.balanceOf(accounts[0])).toString()
      expect(b0).to.equal('100001000000000000000000000')

      await es.stake('1000000000000000000000')
      await plt.generateTokens(es.address, '1000000000000000000000')
      p = (await es.getPricePerFullShare()).toString()
      expect(p).to.equal('2000000000000000000')

      console.log("pool plt: ", (await plt.balanceOf(es.address)).toString())
      console.log("total xplt: ", (await xplt.totalSupply()).toString())

      await es.stake('2000000000000000000000', {from:accounts[1]})
      l1 = (await xplt.balanceOf(accounts[1])).toString()
      expect(l1).to.equal('1000000000000000000000')

      await plt.generateTokens(es.address, '1000000000000000000000')
      console.log("a1 plt: ", (await plt.balanceOf(accounts[1])).toString())
      await es.claim('500000000000000000000',{from:accounts[1]})

      b1 = (await plt.balanceOf(accounts[1])).toString()

      expect(b1).to.equal('99999250000000000000000000')//-2000+1250
    })

  })
})
