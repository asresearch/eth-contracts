const { expect } = require('chai');
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');


const TokenAuctionFactory = artifacts.require("TokenAuctionFactory")
const TokenAuction = artifacts.require("TokenAuction")
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TokenBankV2 = artifacts.require("TokenBankV2");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");

const getBlockNumber = require('./blockNumber')(web3)


contract("Token Auction", (accounts)=>{
  context("init", ()=>{
    let tlist = {};
    let rds = {}
    let bank = {}
    let usdt = {}
    let i = 0
    let start_block = 0;
    it("init", async function(){
      token_factory = await ERC20TokenFactory.deployed()

      tlist_factory = await TrustListFactory.deployed();

      tx = await tlist_factory.createTrustList(accounts.slice(0, 1))
      tlist = await TrustList.at(tx.logs[0].args.addr);

      tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "plt", 18, "PLT", true);
      rds = await ERC20Token.at(tx.logs[0].args._cloneToken);
      await rds.changeTrustList(tlist.address);
      tlist_factory = await TrustListFactory.deployed();

      tx = await tlist_factory.createTrustList(accounts.slice(0, 1));
      tlist = await TrustList.at(tx.logs[0].args.addr);
      bank_factory = await TokenBankV2Factory.deployed();

      tx = await bank_factory.newTokenBankV2("test");
      bank = await TokenBankV2.at(tx.logs[0].args.addr);
      await bank.changeTrustList(tlist.address);

      tx = await bank_factory.newTokenBankV2("test_rec");
      rec_bank = await TokenBankV2.at(tx.logs[0].args.addr);
      await rec_bank.changeTrustList(tlist.address)


      tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "usdt", 6, "USDT", true);
      usdt = await ERC20Token.at(tx.logs[0].args._cloneToken);
      await usdt.changeTrustList(tlist.address)
      await tlist.add_trusted(accounts[0])

      usdt.generateTokens(accounts[0],1000000000);
      usdt.generateTokens(accounts[1],1000000000);
      usdt.generateTokens(accounts[2],1000000000);

      await rds.generateTokens(bank.address, "100000000000000000000000");

      console.log('bank add:', bank.address)
      console.log('rev add:', rec_bank.address)

      auction_factory = await TokenAuctionFactory.deployed()
      tx = await auction_factory.createTokenAuction(bank.address, rds.address, usdt.address, rec_bank.address, 10000000, 20, 1000, '10000000000000000000')
      rds_auction = await TokenAuction.at(tx.logs[0].args.addr);
      await tlist.add_trusted(rds_auction.address);

      await usdt.approve(rds_auction.address, 20000000000, {from:accounts[0]});
      await usdt.approve(rds_auction.address, 10000000000, {from:accounts[1]});
      await usdt.approve(rds_auction.address, 10000000000, {from:accounts[2]});

      await rds_auction.auction('20000000000000000000', 20000000);
      start_block = await getBlockNumber();
      b0 = await usdt.balanceOf(accounts[0]);
      expect(b0.toNumber()).to.equal(600000000);
      await rds_auction.auction('10000000000000000000',50000000,{from:accounts[1]});
      b0 = await usdt.balanceOf(accounts[0]);
      b1 = await usdt.balanceOf(accounts[1]);
      expect(b0.toNumber()).to.equal(1000000000);
      expect(b1.toNumber()).to.equal(500000000);

      while(i <= start_block + 20){
        //Ganache will increase block number for each transaction
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
        i = (await web3.eth.getBlock("latest")).number;
      }
      await rds_auction.auction('10000000000000000000',10000000,{from:accounts[2]});
      r1 = await rds.balanceOf(accounts[1]);
      start_block = await getBlockNumber();
      assert.equal(r1.toString(),'10000000000000000000');
      m = await usdt.balanceOf(rec_bank.address);
      expect(m.toNumber()).to.equal(500000000);
    })
    it("round 1", async function(){
      await expectRevert(rds_auction.end_current_auction(), "not ready to end")
      await expectRevert(rds_auction.auction(100,333333333), "less than minimal amount")
      await expectRevert(rds_auction.auction('100000000000000000000',100), "less than start price")
      await expectRevert(rds_auction.auction('10010000000000000000',10010000), "total payment bump up too small")
      await expectRevert(rds_auction.auction("100000000000000000000000",100000000),"bank doesn't have enough token");
      await rds_auction.pause_auction();
      r2 = await rds.balanceOf(accounts[2]);
      assert.equal(r2.toString(),'10000000000000000000');
      m = await usdt.balanceOf(rec_bank.address);
      expect(m.toNumber()).to.equal(600000000);

      while(i <= start_block + 20){
        //Ganache will increase block number for each transaction
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
        i = (await web3.eth.getBlock("latest")).number;
      }
      await expectRevert(rds_auction.auction('20000000000000000000',20010000),"auction already paused");
    })
    it("round 2", async function(){
      await rds_auction.resume_auction();
      u0 = await usdt.balanceOf(accounts[0]);
      console.log("round 2,usdt 0:", u0.toNumber());
      await rds_auction.auction('20000000000000000000',20000000);
      start_block = await getBlockNumber();
      m = await usdt.balanceOf(rec_bank.address);
      expect(m.toNumber()).to.equal(600000000);
      while(i <= start_block + 20){
        //Ganache will increase block number for each transaction
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
        i = (await web3.eth.getBlock("latest")).number;
      }
      await rds_auction.end_current_auction();
      m = await usdt.balanceOf(rec_bank.address);
      expect(m.toNumber()).to.equal(1000000000);
      r0 = await rds.balanceOf(accounts[0]);
      assert.equal(r0.toString(),'20000000000000000000');
    })
  })
})
