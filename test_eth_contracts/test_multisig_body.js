const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
//const web3 = require("web3")

const MultiSigBody = artifacts.require("MultiSigBody")
const MultiSig = artifacts.require("MultiSig");
const MultiSigBodyFactory = artifacts.require("MultiSigBodyFactory")
const MultiSigFactory = artifacts.require("MultiSigFactory");
const {StepRecorder} = require("../util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");

contract("MultiSigBody", (accounts) =>{
  let msb = {}

  let sr = {}

  let td = {}
  let multisig = {}
  let bank = {}

  it("init", async() =>{
    //sr = StepRecorder("ganache", "multisig");
    //csr = StepRecorder("ganache", "common");
    //multisig = await MultiSig.at(csr.read("multisig"));
    //bank = await TokenBankV2.at(csr.read("tokenbank"));

    mfactory = await MultiSigFactory.deployed()
    tx = await mfactory.createMultiSig(accounts.slice(0, 3))
    multisig = await MultiSig.at(tx.logs[0].args.addr)

    mbfactory = await MultiSigBodyFactory.deployed()
    tx = await mbfactory.createMultiSig(multisig.address, "0x0000000000000000000000000000000000000000")
    msb = await MultiSigBody.at(tx.logs[0].args.addr);
    await multisig.add_multisig_proxy("1", msb.address, {from:accounts[0]})
    await multisig.add_multisig_proxy("1", msb.address, {from:accounts[1]})
    console.log("msb address: ", msb.address)

    tlist_factory = await TrustListFactory.deployed();

    tx = await tlist_factory.createTrustList(accounts.slice(0, 1))
    td = await TrustList.at(tx.logs[0].args.addr);

    await td.transferOwnership(msb.address);

  })

  it("test call with multisig owner", async() =>{
    invoke_id = await multisig.get_unused_invoke_id("call_contract", {from:accounts[0]});
    //data = td.add_trusted(accounts[1]).encodeABI();
data = web3.eth.abi.encodeFunctionCall({
      name: 'add_trusted',
      type: 'function',
      inputs: [
        {
          "name": "addr",
          "type": "address"
        }]
    }, [accounts[1]]);
    await msb.call_contract(invoke_id.toString(), td.address, data, 1, {from:accounts[0]})
    await msb.call_contract(invoke_id.toString(), td.address, data, 0, {from:accounts[1]})
    //expect(true).to.equal(false);
  })
})
