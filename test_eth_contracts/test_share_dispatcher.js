const ShareProof = artifacts.require("ShareProof");
const ShareProofFactory = artifacts.require("ShareProofFactory");
const ShareDispatcher = artifacts.require("ShareDispatcher");
const ShareDispatcherFactory = artifacts.require("ShareDispatcherFactory");
const TrustList = artifacts.require("TrustList");
const TrustListFactory = artifacts.require("TrustListFactory");
const TokenBankV2 = artifacts.require("TokenBankV2");
const AddressArray = artifacts.require("AddressArray");
const SafeMath = artifacts.require("SafeMath");
const getBlockNumber = require('./blockNumber')(web3)
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");


contract('Share_dispatcher', (accounts) =>{
    context("init", ()=>{
        let cb = 0;
        it("Initial", async function(){
          bank = await TokenBankV2.deployed();
          tlist_factory = await TrustListFactory.deployed();

          tx = await tlist_factory.createTrustList(accounts.slice(0, 1))
          tlist = await TrustList.at(tx.logs[0].args.addr);
          await bank.changeTrustList(tlist.address);

          token_factory = await ERC20TokenFactory.deployed()
          tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
            0, "RDS", 18, "RDS", true);
          rds = await ERC20Token.at(tx.logs[0].args._cloneToken);
          await rds.changeTrustList(tlist.address);

          sd_factory = await ShareDispatcherFactory.deployed()
          sp_factory = await ShareProofFactory.deployed();

          tx = await sp_factory.createShareProof("rdss", 18, "RDSS")
          sp = await ShareProof.at(tx.logs[0].args._cloneToken)

            sb = await getBlockNumber();
          tx = await sd_factory.createShareDispatcher("dispatcher", bank.address, rds.address, sp.address, 42000, sb + 100)
              sd = await ShareDispatcher.at(tx.logs[0].args.addr)
          await tlist.add_trusted(sd.address);

            //sd = await ShareDispatcher.deployed();
            await rds.generateTokens(bank.address, 1000001);
            await sp.generateProof(accounts[0], 20);
            await sp.generateProof(accounts[1], 10);
            await sp.generateProof(accounts[2], 30);
            await rds.transfer(accounts[0], 0, {from:accounts[0]});
            await rds.transfer(accounts[0], 0, {from:accounts[0]});
            await sp.generateProof(accounts[0], 40);
        })
        it("Claim", async function(){
            sb = await sd.start_block();
            let i = await getBlockNumber();
            while(i <= sb){
                //Ganache will increase block number for each transaction
                await rds.transfer(accounts[0], 0, {from:accounts[0]});
                i = (await web3.eth.getBlock("latest")).number;
              }
            await rds.transfer(accounts[0], 0, {from:accounts[0]});
            cb = await getBlockNumber();
            expect(cb).to.equal(sb.toNumber()+2);

            await sd.claim_share(accounts[0]);
            b0 = await rds.balanceOf(accounts[0]);
            expect(b0.toNumber()).to.equal(75600);
            await sd.claim_share(accounts[3], {from:accounts[3]});
            b3 = await rds.balanceOf(accounts[3]);
            expect(b3.toNumber()).to.equal(0);
            await sp.destroyProof(accounts[0], 50);

            await sd.claim_share(accounts[0]);
            b0 = await rds.balanceOf(accounts[0]);
            expect(b0.toNumber()).to.equal(134400);

            await sd.claim_share(accounts[1], {from:accounts[1]});
            b1 = await rds.balanceOf(accounts[1]);
            expect(b1.toNumber()).to.equal(37800);
        })
        it("Pause", async function(){
            await sd.change_employee_status(accounts[0], true);
            await sd.change_employee_status(accounts[1], true);
            await sp.destroyProof(accounts[2], 10);
            await sp.destroyProof(accounts[2], 10);
            await sp.destroyProof(accounts[2], 10);

            await sd.claim_share(accounts[2], {from:accounts[2]});
            b2 = await rds.balanceOf(accounts[2]);
            expect(b2.toNumber()).to.equal(224000);

            await sd.change_employee_status(accounts[1], false);
            await sd.claim_share(accounts[0], {from:accounts[0]});
            b0 = await rds.balanceOf(accounts[0]);
            expect(b0.toNumber()).to.equal(151200);

            await sd.change_employee_status(accounts[0], false);
            await sd.claim_share(accounts[0], {from:accounts[0]});
            b0 = await rds.balanceOf(accounts[0]);
            expect(b0.toNumber()).to.equal(172200);

            await sd.claim_share(accounts[1], {from:accounts[1]});
            b1 = await rds.balanceOf(accounts[1]);
            expect(b1.toNumber()).to.equal(138600);

            rem = await sd.remains_from_pause();
            expect(rem.toNumber()).to.equal(200200);

            tua = await sd.total_unclaimed_amount();
            expect(tua.toNumber()).to.equal(21000);
        })
    })
})
