import os
import pickle
from inspect import currentframe, getouterframes

class TxTrace:
    def __init__(self, fp):
        self.fp = fp

    def __enter__(self):
        self.is_dirty = False
        self.data = {}
        if not os.path.exists(self.fp):
            return self

        with open(self.fp, "rb") as f:
            self.data = pickle.load(f)
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.is_dirty:
            with open(self.fp, "wb") as f:
                pickle.dump(self.data, f, protocol = pickle.HIGHEST_PROTOCOL)

        if not (exc_type is None):
            raise Exception('type: {}, value: {}'.format(exc_type, exc_value)).with_traceback(exc_tb)

    def track(self, fn):
        frames = getouterframes(currentframe())
        frameinfo = frames[1]
        pos = ""
        for frameinfo in frames:
            pos = pos + frameinfo.filename + str(frameinfo.lineno) + str(frameinfo.function) + str(frameinfo.code_context)

        if pos in self.data:
            return self.data[pos]
        else:
            r = fn()
            self.data[pos] = r
            self.is_dirty = True
            return r

    def clear(self):
        if os.path.exists(self.fp):
            os.remove(self.fp)


