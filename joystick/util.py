import os
import json

def read_abi(path):
    with open(path) as f:
        d = json.load(f)
        return d["abi"]
