import os
import json
from joystick import util
import types
import eth_account
from inspect import signature
import time

class interact_call_wrapper(object):
    def __init__(self, web3, func_name, pthis, old_func):
        self.web3 = web3
        self.pthis = pthis
        self.old_func = old_func
        self.func_name = func_name

class function_call_wrapper(object ):
    def __init__(self, web3, func_name, old_func):
        self.web3 = web3
        self.old_func = old_func
        self.func_name = func_name

def new_transact(self, transaction):
    if 'from' in transaction and type(transaction['from']) == eth_account.signers.local.LocalAccount:
        account = transaction['from']

        if not 'chainId' in transaction:
            transaction['chainId'] = self.web3.eth.chain_id

        if not 'nonce' in transaction:
            nonce = self.web3.eth.get_transaction_count(account.address)
            transaction['nonce'] = nonce

        if not 'gasPrice' in transaction:
            transaction['gasPrice'] = self.web3.eth.gasPrice

        transaction['from'] = account.address
        tx = self.pthis.buildTransaction(transaction)
        signed_tx = self.web3.eth.account.signTransaction(tx, private_key=account.privateKey)
        tx_hash = self.web3.eth.sendRawTransaction(signed_tx.rawTransaction)
    else:
        tx_hash = self.old_func(transaction)

    print(time.ctime(time.time()), ' waiting for', self.func_name, ' hash: ', tx_hash.hex())
    tx_receipt = self.web3.eth.wait_for_transaction_receipt(tx_hash)
    print(time.ctime(time.time()), ' tx ', tx_hash.hex(), ' got receipt')
    return tx_receipt



def new_function_call(self, *args, **kwargs):
    ret = self.old_func(*args, **kwargs)
    old = getattr(ret, "transact")

    setattr(ret, "transact", types.MethodType(new_transact, interact_call_wrapper(self.web3, self.func_name, ret, old)))
    return ret

class contract(object):
    def __init__(self, web3, name, fp, address):
        self.web3 = web3
        self.name = name
        self.fp = fp
        self.address = address
        self.native_contract = web3.eth.contract(address=address, abi = util.read_abi(self.fp))
        self.functions = self.native_contract.functions
        self.events = self.native_contract.events
        self.add_support_to_account()

    def add_support_to_account(self):
        for item in self.functions:
            old = getattr(self.functions, item)
            setattr(self.functions, item, types.MethodType(new_function_call, function_call_wrapper(self.web3, self.name +"::"+ item, old)))

