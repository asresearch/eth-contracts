import os
import types
from joystick.contract import contract
current_file=os.path.abspath(__file__)
current_dir=os.path.dirname(current_file)
contracts_path = os.path.join(current_dir, "../build/contracts")

class contracts_holder(object):
    pass

def get_all_json_files_in_dir(fp):
    ret = []
    for item in os.listdir(fp):
        if item.endswith(".json") and not item.endswith(".abi.json"):
            ret.append(os.path.join(fp, item))
    return ret

class contract_creator_wrapper(object):
    def __init__(self, web3, name, fp):
        self.web3 = web3
        self.name = name
        self.fp = fp

def contract_creator(self, address):
    return contract(self.web3, self.name, self.fp, address)

def attach_function(holder, web3, fp):
    name = os.path.basename(fp)
    name = name[0:name.find('.json')]
    setattr(holder, name, types.MethodType(contract_creator, contract_creator_wrapper(web3, name, fp)))


def import_all_contracts(web3):
    holder = contracts_holder()
    contracts = get_all_json_files_in_dir(contracts_path)

    for item in contracts:
        attach_function(holder, web3, item)

    return holder
