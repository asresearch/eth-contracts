import json
import os
class StepRecorder(object):
    def __init__(self, network, name):
        self.network = network
        self.name = name
        fname = self.network + '-'+name + '.json'
        with open(os.path.join(os.getcwd(), fname), 'r') as f:
            self.json = json.load(f)

    def read(self, name):
        return self.json[self.network][name]
