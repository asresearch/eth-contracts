import sys
import os
current_file=os.path.abspath(__file__)
current_dir=os.path.dirname(current_file)
sys.path.append(os.path.dirname(current_dir))

from joystick import *
from web3 import Web3

w3 = Web3(Web3.HTTPProvider("URL_HERE"))

js = import_all_contracts(w3)

tm = js.TokenManagement('ADDRESS_HERE')
private_key="PRIVATE_KEY_HERE"
old_owner =tm.functions.owner().call()
print('old owner:', old_owner)
owner=w3.eth.account.privateKeyToAccount(private_key)
tx = tm.functions.transferOwnership(owner.address).transact({'gas':7000000, 'gasPrice':w3.eth.gasPrice, 'from':owner})
# tx = tm.functions.transferOwnership(owner.address).transact({'gas':7000000, 'gasPrice':w3.eth.gasPrice, 'from':w3.eth.accounts[0]})
print(tx)

