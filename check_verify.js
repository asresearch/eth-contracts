const fs = require("fs");
const qs = require('qs');
const { execSync  } = require('child_process');
const axios = require('axios');
require('dotenv').config()
var Contract = require('web3-eth-contract');
const Web3 = require('web3')

var web3 = new Web3('http://localhost:8545')

function wait(second) {
  let ChildProcess_ExecSync = require('child_process').execSync;
  ChildProcess_ExecSync('sleep ' + second);
};


async function check(filename){
    v_data = {'items':[]}
    if(fs.existsSync(filename)){
      v_data = JSON.parse(fs.readFileSync(filename, 'utf-8').toString());
    }else{
      console.error("file ", filename , " not exist")
      return;
    }
  //console.log(v_data)

  for(let item of v_data["items"]){
    if(typeof item["guid"] === 'undefined' || item["guid"] === null){
      continue
    }
    if(item["guid"].length < 10){
      continue
    }
    if(item['guid'] ==="Contract source code already verified"){
      continue
    }
    console.log("checking status of contract ", item['name'], " at address ", item["address"], " GUID ", item['guid'])
    status_obj = {
      apikey:process.env.ETHERSCAN_API_KEY,
      module:'contract',
      action:'checkverifystatus',
      guid:item['guid']
    }
    msg = await  axios.post(item["api_url"], qs.stringify(status_obj), {headers:{
            'Content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    }}).then(function (response) {
      //console.log(response)
        return response.data['result']
    })

    if(msg ==="Pass - Verified"){
        console.log('verified contract ', item["name"], ' at address ', item["address"])
    }else{
        console.log("xxx", msg)
    }
      wait(1)
  }
}

const myArgs = process.argv.slice(2);
if(myArgs.length == 0){
  console.log("missing filename")
}else{
  (async function(){
    await check(myArgs[0])
  })()
}
