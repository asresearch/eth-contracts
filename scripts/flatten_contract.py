import os


current_file = os.path.abspath(__file__)
current_dir = os.path.dirname(current_file)
contract_dir = os.path.join(current_dir, "../contracts/")
flatten_dir = os.path.join(current_dir, "../build/contracts/flattened/")


def gen_flattened_file(fp, name):
    filename = os.path.join(fp, name)
    print('processing {}'.format(filename))
    os.system('mkdir -p {}'.format(flatten_dir))
    os.system(
        'truffle-flattener {0} > {1}'.format(filename, os.path.join(flatten_dir, name)))


def get_all_contract_files_in_dir(abs_dir):
    for root, dirs, files in os.walk(abs_dir):
        for f in files:
            if f.endswith('.sol'):
                gen_flattened_file(root, f)


get_all_contract_files_in_dir(contract_dir)
