const {DHelper} = require("../util.js");
const MultiSigFactory = artifacts.require("MultiSigFactory");
const MultiSigBodyFactory = artifacts.require("MultiSigBodyFactory")

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(MultiSigFactory)
  await dhelper.readOrCreateContract(MultiSigBodyFactory)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
