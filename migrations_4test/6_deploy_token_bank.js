const {DHelper} = require("../util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TestBankFactory = artifacts.require("TestBankFactory");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(TokenBankV2Factory)
  await dhelper.readOrCreateContract(TokenBankV2, [], "test")
  await dhelper.readOrCreateContract(TestBankFactory)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
