const {DHelper} = require("../util.js");
const TokenAuctionFactory = artifacts.require("TokenAuctionFactory");
const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const ShareDispatcherFactory = artifacts.require("ShareDispatcherFactory");
const ShareProofFactory = artifacts.require("ShareProofFactory");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(TokenAuctionFactory, [SafeMath])
  await dhelper.readOrCreateContract(ShareDispatcherFactory, [SafeMath, AddressArray])
  await dhelper.readOrCreateContract(ShareProofFactory, [SafeMath, AddressArray])
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
