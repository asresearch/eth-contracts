const {DHelper} = require("../util.js");
const MerkleDropFactory = artifacts.require("MerkleDropFactory");
const MerkleProof = artifacts.require("MerkleProof");
const SafeMath = artifacts.require("SafeMath");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(MerkleDropFactory, [SafeMath, MerkleProof])
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
