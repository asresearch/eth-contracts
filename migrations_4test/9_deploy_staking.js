const {DHelper} = require("../util.js");
const ERC20StakingFactory = artifacts.require("ERC20StakingFactory");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(ERC20StakingFactory, [SafeMath, SafeERC20])
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
