const {DHelper} = require("../util.js");
const MerkleDropV2Factory = artifacts.require("MerkleDropV2Factory");
const MerkleProof = artifacts.require("MerkleProof");
const SafeMath = artifacts.require("SafeMath");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(MerkleDropV2Factory, [SafeMath, MerkleProof])
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
